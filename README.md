
Matériel nécessaire au bon fonctionnement du script :
Un ordinateur possédant une connexion réseau stable et 80Go de mémoire disponible

Paquets apt nécessaires
- p7zip
- g++ >= 8
- openjdk-7-jdk


LE DESCRIPTIF DU PROJET

= Communautés dans les contributeurs de Wikipédia =

Les contributeurs de wikipédia sont en général des personnes spécialisées dans un ou plusieurs domaines (langages formels, physique nucléaire, histoire du Mozambique…). On peut donc s'attendre à ce que les pages modifiées par un même utilisateur aient un lien logique, et donc aussi à ce que deux pages proches aient des contributeurs communs.

== Données utiles ==

Dans une première approche, les données de wikipédia qui nous intéressent sont, pour chaque page, le nombre de modification par utilisateur.

Cependant il pourra nnous être utile d'avoir accès aux Catégories, Portails, et Pages Similaires de wikipédia, qui pourront nous aider à établir des liens a priori entre des pages.

Enfin, il pourra être pertinent de ne pas considérer (ou de moins considérer) les modifications de type typo, qui peuvent être le fait d'un simple utilisateur zélé modifiant les pages à la lecture.

== Modélisation ==

On pourra représenter les données comme un graphe, de deux manières différentes

Un graphe dont les sommets sont les utilisateurs, et où deux utilisateurs sont reliés par une arête s'ils ont contribué à une page en commun. On pourra bien sûr pondérer les arêtes en fonction du nombre de modifiactions pour la page, du nombre de page en commun.

Un graphe dont les sommets sont les pages, et où deux pages sont reliées par une arête si elles ont un contributeur en commun. On pourra pondérer les arêtes en fonction du nombre de contributions de ces utilisateurs, du nombre d'utilisateurs communs, mais également en fonction des Catégories, Portails, Pages Similaires.

== Algorithmie ==

On pourra essayer d'appliquer du travail déjà fait dans la recherche de communautés dans des graphes.
