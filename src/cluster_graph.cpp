#include <iostream>
#include <cmath>
#include <algorithm>
#include <map>
#include <vector>
#include <fstream>
#include <unordered_set>
#include <cstdlib>

typedef long long ll;
using namespace std;
std::string::size_type sz;

string delimiter1=" @@@@@ ";
int len1 = delimiter1.length();
string delimiter2=" _____ ";
int len2 = delimiter2.length();

map<string,map<string,double>> parse_file(ifstream &input){
  map<string,map<string,double>> graph;
  string line;
  string page;
  while (input){
    getline(input, line);
    int pos = line.find(delimiter1);
    page = line.substr(0, pos);
    line.erase(0, pos+len1);
    while (line.size()) {
      pos = line.find(delimiter1);
      string edge;
      if (pos == -1) {
        edge = line;
        line = "";
      }
      else {
        edge = line.substr(0, pos);
        line.erase(0, pos+len1);
      }

      pos = edge.find(delimiter2);
      string neigh = edge.substr(0, pos);
      edge.erase(0, pos+len2);
      double weight = stof(edge, &sz);
      graph[page][neigh] = weight;
      //cout << page << "     " << neigh << "     " << weight << endl;
    }
  }
  return graph;
}

void find_heaviest_edge_and_merge(
    map<string,map<string,double>> &graph,
    map<string,unordered_set<string>> &clusters
    ){
  int wmax = -1;
  string v1, v2;
  for (auto const &x : graph) {
    for (auto const &y : x.second) {
      if (x.first == y.first) break;
      if (y.second > wmax) {
        wmax = y.second;
        v1 = x.first;
        v2 = y.first;
      }
    }
  }
  // on va mettre toutes les arêtes concernant v2 vers v1,
  // et se souvenir que v2 est dans le cluster de v1
  clusters[v1].insert(v2);
  for (auto const &y : graph[v2]) {
    graph[v1][y.first] += y.second;
  }
  graph.erase(v2);
  for (auto const &x : graph) {
    if (x.first == v1) break;
    graph[x.first][v1] = max (graph[x.first][v2], graph[x.first][v1]);
    (graph[x.first]).erase(v2);
  }
  graph[v1].erase(v2);
  cout << v1 << "   " << v2 << endl;
}

int main (int argc, char *argv[]){
  if (argc != 4){
    cerr << "You must use exactly 3 arguments" << endl;
    cerr << "nb clusters, nb_outliers, input_file" << endl;
    exit(EXIT_FAILURE);
  }
  int k_clusters = atoi(argv[1]);
  int z_outliers = atoi(argv[2]);
  map<string,unordered_set<string>> clusters;
  ifstream input(argv[3], ios::in);
  map<string,map<string,double>> graph = parse_file(input);
  input.close();
  for (int i = 0 ; i < 200 ; i++) {
    find_heaviest_edge_and_merge(graph, clusters);
  }
  return 0;
}
