#include <iostream>
#include <cmath>
#include <algorithm>
#include <map>
#include <vector>
#include <fstream>
#include <unordered_set>
#include <cstdlib>

typedef long long ll;

using namespace std;
std::string::size_type sz;

int main(int argc, char *argv[]){
  
  std::string delimiter = " @@@@@@@ ";
  
  map<string,map< string, double > > graph;
  
  int pos;
  
  //ofstream output("graphage_full_pondere", ios::out | ios::trunc);
  ifstream input(argv[1], ios::in);
  
  vector<string> aux;
  int n;
  double n_modifs;
  int compteur = 0;
  for( std::string line; getline( input, line ) ; ){
    aux.clear();
    pos = line.find(delimiter);
    n_modifs = stof(line.substr(0, pos),&sz);
    line.erase(0, pos + delimiter.length());
    pos = line.find(delimiter);
    line.erase(0, pos + delimiter.length());
      while(line.size()){
        if(n_modifs > 100) break;
        pos = line.find(delimiter);
        if(pos == -1) {
          aux.push_back(line);
          break;
        }
        aux.push_back(line.substr(0, pos));
        line.erase(0, pos + delimiter.length());
      }
    n = aux.size();
    for(int i = 0; i < n-1; ++i){
      for(int j = i+1; j<n; ++j){
        (graph[aux[i]])[aux[j]] += 1/sqrt(n_modifs);
        (graph[aux[j]])[aux[i]] += 1/sqrt(n_modifs);
      }
    }
  }
  
  
    input.close();
  
  for(auto const& element : graph){
      cout << element.first;
      for(auto const& nom_page : element.second){
        cout << " @@@@@ " << nom_page.first << " _____ " << nom_page.second;
      }
      cout << endl;
  }
  //output.close();
  
  
  return 0;
}


