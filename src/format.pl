#!/usr/bin/perl

use strict;
use warnings;

open(FILE_IN,$ARGV[0]);
my @contenu = <FILE_IN>;
close(FILE_IN);

open(FILE_OUT,">$ARGV[0]");
foreach my $line (@contenu)
{
  $line =~ s/\<a href=\"(.*)\"\>.*/$1/;
  if ( $line =~ m/.*7z$/ ) {
    print FILE_OUT "$line";
  }
}
close(FILE_OUT);
