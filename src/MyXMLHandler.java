import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class MyXMLHandler extends DefaultHandler{

   private String node = null;
   private boolean contrib = false;
   private String page = null;
   private boolean ouvert = false;

   public void startDocument() throws SAXException {
    System.out.println("Beginning of the parsing")
   }
   public void endDocument() throws SAXException {
    System.out.println("Ending of the parsing")
   }

  // Redéfinition des méthodes pour intercepter les événements
  public void startElement(String namespaceURI, String lname,
      String qname, Attributes attrs) throws SAXException {
    // Cette variable contient le nom du nœud qui a créé l'événement
    node = qname;
    if (qname == "title") {
      ouvert = true;
    }

    // Cette dernière contient la liste des attributs du nœud
   	if (attrs != null) {
      for (int i = 0; i < attrs.getLength(); i++) {
        // nous récupérons le nom de l'attribut
        String aname = attrs.getLocalName(i);
        }
   		}
    }

  //permet de récupérer la valeur d'un nœud
  public void characters(char[] data, int start, int end){
    //La variable data contient tout notre fichier.
    //Pour récupérer la valeur, nous devons nous servir des limites en paramètre
    //"start" correspond à l'indice où commence la valeur recherchée
    //"end" correspond à la longueur de la chaîne
   	String str = new String(data, start, end);
    if (ouvert && node == "title") {
      page = str;
      ouvert = false;
    }
    if (contrib && (node == "ip" || node == "id")) {
      System.out.println(page + " @@@@@@ " + node + " @@@@@@ " + str);
      contrib = false;
    }
    if (node == "contributor") contrib = true;
  }
}
