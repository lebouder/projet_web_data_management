#! /bin/bash

pattern="wiki-latest-pages-meta-history"
all_dumps="dumps.html"
urls="urls.txt"
wget_log="wget.log"

function usage () {
  echo -n "
  -h --help       displays this help
  -l --language   to change the wiki to download (default: french)
  -c --clean      to clean logs and previous graphs
  -d --download   to download again the list of urls
  -r --run        to run
  -a --all        to do all of this
"
}

function clean () {
  echo Cleaning logs
  rm -f $wget_log
  echo Cleaning graph
  rm -f ../build/*.txt
  echo Cleaning bin
  rm -f Main.class MyXMLHandler.class parse_contrib
}

function compile () {
  echo Compiling everything
  javac MyXMLHandler.java || ( echo fail ; exit 1; )
  javac Main.java || ( echo fail ; exit 1; )
  g++ parse_contrib.cpp -o parse_contrib
  g++ create_graph.cpp -o create_graph
  g++ cluster_graph.cpp -o cluster_graph
}

function extract_list () {
  echo Extracting main page $root_url
  wget $root_url -O $all_dumps -a $wget_log

  echo Extracting the URLs
  grep $pattern $all_dumps > $urls
  ./format.pl $urls
}

function run () {
  while read line; do
    echo Extracting zipped file $line
    wget $root_url$line -O $line -a $wget_log

    echo Unzipping $line
    7z e $line

    echo Running the XML parser on it
    java -DentityExpansionLimit=2147480000 -DtotalEntitySizeLimit=2147480000 -Djdk.xml.totalEntitySizeLimit=2147480000 Main ${line/.7z/} > "../build/graph_"${line/.7z/.txt}

    rm -f $line ${line/.7z/}
  done < $urls

  ./parse_contrib ../build/graph_* > ../build/edges
  ./create_graph ../build/edges > ../build/graph
}


language="fr"
clean=
compile=
dl=
run=

while [ "$1" != "" ]; do
    case $1 in
        -l | --language )       shift
                                language=$1
                                ;;
        -e | --erase   )        clean=1
                                ;;
        -c | --compile )        compile=1
                                ;;
        -d | --download )       dl=1
                                ;;
        -r | --run )            run=1
                                ;;
        -a | --all )            clean=1
                                compile=1
                                dl=1
                                run=1
                                ;;
        -h | --help )           usage
                                exit 1
                                ;;
        * )                     usage
                                exit 1
    esac
    shift
done

root_url="https://dumps.wikimedia.org/"$language"wiki/latest/"

if [ -n "$clean" ]; then clean; fi
if [ -n "$compile" ]; then compile; fi
if [ -n "$dl" ]; then extract_list; fi
if [ -n "$run" ]; then run; fi

